import unittest
import csv
import os.path as path

CSV_DIR = path.realpath(path.join(path.dirname(path.realpath(__file__)), '../csv'))
CURRENCY_FILE = path.join(CSV_DIR, 'currencies.csv')

class ValuationTestBase(unittest.TestCase):
    def setUp(self):
        with open(CURRENCY_FILE) as currencies_data:
            currencies_reader = csv.reader(currencies_data)
            currencies_reader.__next__() # skip header
            self.currencies_ratios = dict(currencies_reader)
