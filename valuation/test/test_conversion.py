import unittest
from valuation import valuation as v
from . import valuation_testbase as base

class PriceConvertionTestCase(base.ValuationTestBase):
    def test_convert(self):
        price = 1000
        currency = 'GBP'
        ratio = float(self.currencies_ratios[currency])

        actual_converted_price = v.convertToPLN(price, currency)
        expected_converted_price = price * ratio

        self.assertEqual(
            actual_converted_price,
            expected_converted_price,
            'incorrect converted price for currency {0}'.format(currency))

    def test_invalid_currency(self):
        price = 1000
        currency = 'ASDF'
        try:
            v.convertToPLN(price, currency)
        except ValueError:
            pass
        else:
            self.fail('expected invalid currency exception')

    def test_zero_price(self):
        price = 0
        for currency in self.currencies_ratios:
            actual_converted_price = v.convertToPLN(price, currency)
            expected_converted_price = 0

            self.assertEqual(
                actual_converted_price,
                expected_converted_price,
                'price not zero for currency {0}'.format(currency))

    def test_pln_to_pln(self):
        price = 1000
        currency = 'PLN'
        actual_converted_price = v.convertToPLN(price, currency)
        expected_converted_price = price

        self.assertEqual(
            actual_converted_price,
            expected_converted_price,
            'price not identical after conversion')


if __name__ == '__main__':
    unittest.main()
