import unittest
from unittest.mock import patch
from valuation import valuation as v
from . import valuation_testbase as base

class TopProductsExtractionTestCase(base.ValuationTestBase):
    @patch.object(v, 'readData')
    def test_extract_limits(self, mock_read_data):
        input_data = [
            {
                'id': '1',
                'price': '1000',
                'currency': 'GBP',
                'quantity': '1',
                'matching_id': '1'
            },
            {
                'id': '2',
                'price': '2000',
                'currency': 'GBP',
                'quantity': '1',
                'matching_id': '1'
            }
        ]
        mock_read_data.return_value = input_data

        matching_id = '1'
        top_priced_count = 1

        self.assertTopProducts(input_data[1:], matching_id, top_priced_count, 1)

    @patch.object(v, 'readData')
    def test_extract_only_matching_id(self, mock_read_data):
        input_data = [
            {
                'id': '1',
                'price': '2000',
                'currency': 'GBP',
                'quantity': '1',
                'matching_id': '2'
            },
            {
                'id': '2',
                'price': '1000',
                'currency': 'GBP',
                'quantity': '1',
                'matching_id': '1'
            }
        ]
        mock_read_data.return_value = input_data

        matching_id = '1'
        top_priced_count = 1

        self.assertTopProducts(input_data[1:], matching_id, top_priced_count, 0)


    @patch.object(v, 'readData')
    def test_extract_currency(self, mock_read_data):
        input_data = [
            {
                'id': '1',
                'price': '2000',
                'currency': 'PLN',
                'quantity': '1',
                'matching_id': '1'
            },
            {
                'id': '2',
                'price': '1000',
                'currency': 'GBP',
                'quantity': '1',
                'matching_id': '1'
            }
        ]
        mock_read_data.return_value = input_data

        matching_id = '1'
        top_priced_count = 1

        self.assertTopProducts(input_data[1:], matching_id, top_priced_count, 1)


    @patch.object(v, 'readData')
    def test_extract_quantity(self, mock_read_data):
        input_data = [
            {
                'id': '1',
                'price': '2000',
                'currency': 'PLN',
                'quantity': '1',
                'matching_id': '1'
            },
            {
                'id': '2',
                'price': '1000',
                'currency': 'PLN',
                'quantity': '3',
                'matching_id': '1'
            }
        ]
        mock_read_data.return_value = input_data

        matching_id = '1'
        top_priced_count = 1

        self.assertTopProducts(input_data[1:], matching_id, top_priced_count, 1)

    @patch.object(v, 'readData')
    def test_extract_top_count_larger_than_matches(self, mock_read_data):
        input_data = [
            {
                'id': '2',
                'price': '1000',
                'currency': 'PLN',
                'quantity': '3',
                'matching_id': '1'
            },
            {
                'id': '1',
                'price': '2000',
                'currency': 'PLN',
                'quantity': '1',
                'matching_id': '1'
            }
        ]
        mock_read_data.return_value = input_data

        matching_id = '1'
        top_priced_count = 3

        self.assertTopProducts(input_data[0:], matching_id, top_priced_count, 0)

    def assertTopProducts(self,
                          expected_top_products,
                          matching_id,
                          top_priced_count,
                          expected_ignored_count):

        actual_top_products, actual_ignored_count = v.extractTopProducts(
            matching_id,
            top_priced_count)

        self.assertEqual(actual_top_products, expected_top_products,
            'list of top products does not match the expected')
        self.assertEqual(actual_ignored_count, expected_ignored_count,
            'number of ignored products does not match the expected')

