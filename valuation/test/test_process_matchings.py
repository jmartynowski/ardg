import unittest
from unittest.mock import patch
from valuation import valuation as v
from . import valuation_testbase as base

class MatchProcessingTestCase(base.ValuationTestBase):
    @patch.object(v, 'readData')
    def test_process_quantity(self, mock_read_data):
        input_data = [
            {
                'id': '1',
                'price': '1000',
                'currency': 'PLN',
                'quantity': '3',
                'matching_id': '1'
            }
        ]
        mock_read_data.return_value = input_data

        matching_id = '1'
        top_priced_count = 1 
        
        total_price = float(input_data[0]['price']) * float(input_data[0]['quantity'])
        avg_price = total_price / float(input_data[0]['quantity'])

        expected_ignored_count = 0

        self.assertOutputRow(matching_id, top_priced_count, total_price,
            avg_price, expected_ignored_count)


    @patch.object(v, 'readData')
    def test_process_multiple(self, mock_read_data):
        input_data = [
            {
                'id': '1',
                'price': '1000',
                'currency': 'PLN',
                'quantity': '1',
                'matching_id': '1'
            },
            {
                'id': '1',
                'price': '2000',
                'currency': 'PLN',
                'quantity': '1',
                'matching_id': '1'
            }
        ]
        mock_read_data.return_value = input_data

        matching_id = '1'
        top_priced_count = 2 
        
        total_price = float(input_data[0]['price']) + float(input_data[1]['price'])
        avg_price = total_price / \
            (float(input_data[0]['quantity']) + float(input_data[1]['quantity']))

        expected_ignored_count = 0

        self.assertOutputRow(matching_id, top_priced_count, total_price,
            avg_price, expected_ignored_count)


    def assertOutputRow(self,
                        matching_id,
                        top_priced_count,
                        total_price,
                        avg_price,
                        expected_ignored_count):

        expected_outrow = [
            matching_id,
            '{0:.2f}'.format(total_price),
            '{0:.2f}'.format(avg_price),
            'PLN',
            expected_ignored_count
        ]

        actual_outrow = v.processSingleMatching(matching_id, top_priced_count)
        self.assertEqual(actual_outrow, expected_outrow,
            'output row does not match the expected')
