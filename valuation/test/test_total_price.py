import unittest
from collections import OrderedDict
from valuation import valuation as v
from . import valuation_testbase as base

class TotalPriceCalculationTestCase(base.ValuationTestBase):
    def test_zero_price(self):
        product = self.createTestProduct(0, 'PLN', 1)

        expected_total_price = 0
        actual_total_price = v.getTotalPriceInPLN(product)

        self.assertEqual(
            actual_total_price,
            expected_total_price,
            'total price not zero')

    def test_zero_quantity(self):
        product = self.createTestProduct(1000, 'PLN', 0)

        expected_total_price = 0
        actual_total_price = v.getTotalPriceInPLN(product)

        self.assertEqual(
            actual_total_price,
            expected_total_price,
            'total price not zero')

    def test_single_item(self):
        product = self.createTestProduct(1000, 'PLN', 1)

        expected_total_price = product['price'] * product['quantity']
        actual_total_price = v.getTotalPriceInPLN(product)

        self.assertEqual(
            actual_total_price,
            expected_total_price,
            'total price not calculated correctly')

    def test_multiple_items(self):
        product = self.createTestProduct(1000, 'PLN', 4)

        expected_total_price = product['price'] * product['quantity']
        actual_total_price = v.getTotalPriceInPLN(product)

        self.assertEqual(
            actual_total_price,
            expected_total_price,
            'total price not calculated correctly')

    def test_single_item_currency(self):
        product = self.createTestProduct(1000, 'GBP', 1)

        expected_total_price = \
            product['price'] * \
            product['quantity'] * \
            float(self.currencies_ratios[product['currency']])

        actual_total_price = v.getTotalPriceInPLN(product)

        self.assertEqual(
            actual_total_price,
            expected_total_price,
            'total price not calculated correctly')

    def test_multiple_items_currency(self):
        product = self.createTestProduct(1000, 'GBP', 4)

        expected_total_price = \
            product['price'] * \
            product['quantity'] * \
            float(self.currencies_ratios[product['currency']])

        actual_total_price = v.getTotalPriceInPLN(product)

        self.assertEqual(
            actual_total_price,
            expected_total_price,
            'total price not calculated correctly')

    def createTestProduct(self, price, currency, quantity):
        return OrderedDict([
            ('id', 1),
            ('price', price),
            ('currency', currency),
            ('quantity', quantity),
            ('matching_id', 1)
        ])

if __name__ == '__main__':
    unittest.main()
