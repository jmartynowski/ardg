import csv
import functools
import os.path as path
import textwrap

CSV_DIR = path.realpath(path.join(path.dirname(path.realpath(__file__)), '../csv'))
MATCHINGS_FILE = path.join(CSV_DIR, 'matchings.csv')
DATA_FILE = path.join(CSV_DIR, 'data.csv')
CURRENCY_FILE = path.join(CSV_DIR, 'currencies.csv')
OUTPUT_FILE = path.join(CSV_DIR, 'top_products.csv')
OUTPUT_FIELDNAMES = [
    'matching_id',
    'total_price',
    'avg_price',
    'currency',
    'ignored_products_count'
]


# As there is a total of 180 currencies in the world, the currency csv can
# never be extremely large, so storing this data in a dictionary still seems
# reasonable.
with open(CURRENCY_FILE) as currencies_data:
    currencies_reader = csv.reader(currencies_data)
    currencies_reader.__next__() # skip header
    currencies_ratios = dict(currencies_reader)


def convertToPLN(price, currency):
    """
    Converts a given price to a currency by finding the correct ratio.
    """
    try:
        ratio = currencies_ratios[currency]
    except KeyError:
        raise ValueError('Currency {0} not found!'.format(currency))

    return price * float(ratio)

# In case of data csv, there might be hundreds or thousands of records, so
# storing it in memory may not be a good idea.
def readData():
    """
    Lazily reads the data file returning each line as a dict.
    """
    with open(DATA_FILE) as data:
        data_reader = csv.DictReader(data)

        for row in data_reader:
            yield row


def getTotalPriceInPLN(product):
    """
    Calculates the total price of a given product in PLN, taking its quantity
    into account.
    """
    price = float(product['price'])
    price_pln = 0

    if product['currency'] == 'PLN':
        price_pln = price
    else:
        price_pln = convertToPLN(price, product['currency'])

    return price_pln * float(product['quantity']) # qty is an int but will be cast to float anyway 


def extractTopProducts(matching_id, top_priced_count):
    """
    Returns a specified number of top-priced products matching a given id.
    """
    # Not a perfect solution. See notes near the main script.
    matching_data = [
        product for product in readData() \
            if product['matching_id'] == matching_id]

    matching_data.sort(
        key=getTotalPriceInPLN,
        reverse=True)

    datalength = len(matching_data)
    set_limit = min(datalength, top_priced_count) # handle top_priced_count > number of matches
    ignored_products_count = datalength - set_limit
    return matching_data[:set_limit], ignored_products_count # limit the data set


def processSingleMatching(matching_id, top_priced_count):
    """
    Calculates all values for a single row of output.
    """
    top_products, ignored_products_count = extractTopProducts(matching_id, top_priced_count)

    # Calculate total price and total quantity for all matching products
    total_price, total_qty = functools.reduce(
        lambda price_n_qty, product: (
            price_n_qty[0] + getTotalPriceInPLN(product),
            price_n_qty[1] + int(product['quantity'])
        ),
        top_products,
        (0, 0))

    # Avoid division by zero
    if total_qty == 0:
        raise ValueError('Total product quantity equal to zero for product with ' + \
                'matching id {0}!'.format(matching_id))

    avg_price = total_price / total_qty

    return [
        matching_id,
        '{0:.2f}'.format(total_price),
        '{0:.2f}'.format(avg_price),
        'PLN',
        ignored_products_count
    ]


# This is likely not the most efficient solution, as the data file is
# iterated over for each matching_id. If it were iterated over only once, all
# the matching products would have to be stored in memory, which is feasible
# for a small data file, but with large files could take up a lot of memory.
# The best solution that comes to mind would be to store only very short lists
# containing top priced products and update them on every row of the data file.
# Unfortunately, this would require additional time to implement and test.
if __name__ == '__main__':
    print('Processing files inside \'{0}\' directory...'.format(CSV_DIR))
    with open(MATCHINGS_FILE) as matchings, \
         open(OUTPUT_FILE, 'w') as outfile:

        matchings_reader = csv.reader(matchings)
        outfile_writer = csv.writer(outfile)

        outfile_writer.writerow(OUTPUT_FIELDNAMES) # write header
        matchings_reader.__next__() # skip header

        for matching in matchings_reader:
            matching_id = matching[0]
            top_priced_count = int(matching[1])

            outrow = processSingleMatching(matching_id, top_priced_count)

            outfile_writer.writerow(outrow)

    print('Done. Output written to \'{0}\'.'.format(OUTPUT_FILE))
