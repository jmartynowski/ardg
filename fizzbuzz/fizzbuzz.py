from collections import namedtuple
import textwrap

TEXT_WIDTH = 80
RANGE_MIN = 1
RANGE_MAX = 10000

# For readability. A regular tuple could be used as well.
Divpair = namedtuple('Divpair', ['divisor', 'text'])

divpairs = (
    Divpair(3, 'Fizz'),
    Divpair(5, 'Buzz')
)

def printFizzBuzz(start, stop):
    """
    Loops over a range (inclusive), tests for divisibility by predefined
    values, and prints concatenated strings associated with particular
    divisors. If a current number in the loop is not divisible by any, prints
    the number itself.
    """
    for n in range(start, stop + 1):
        output = ''.join([divpair.text for divpair in divpairs if n % divpair.divisor == 0])
     
        if output: print(output)
        else: print(n)


def inputBounded(prompt, val_min, val_max):
    """
    Prompts the user to enter an integer within given boundary values (inclusive).
    """
    while True:
        try:
            val = int(input(prompt))
            if val < val_min:
                print('[!] Error: the number must be at least ' + str(val_min) + '.')
            elif val > val_max:
                print('[!] Error: the number cannot be greater than ' + str(val_max) + '.')
            else:
                return val
        except ValueError:
            print('[!] Error: input must be a number.')


if __name__ == '__main__':
    straight_line = TEXT_WIDTH * '-'

    info = textwrap.dedent('''\
        Please enter start and end values of the range. Both numbers must fit within
        <{min},{max}>, and the second one must be greater than the first.
        '''.format(min = RANGE_MIN, max = RANGE_MAX) + straight_line
    )

    print(textwrap.fill(info, TEXT_WIDTH))

    n = inputBounded('Start value: ', RANGE_MIN, RANGE_MAX - 1)
    m = inputBounded('End value: ', n + 1, RANGE_MAX)
     
    print('\nOutput:')
    print(straight_line)
    printFizzBuzz(n, m)
